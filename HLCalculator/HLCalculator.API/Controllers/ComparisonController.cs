﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HLCalculator.Business;
using HLCalculator.Domain.LoanComparison;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;


namespace HLCalculator.API.Controllers
{
    [Route("api/v1/calculator")]
    [EnableCors(AppConstants.CorsPolicy)]
    public class LoanCalculatorController : Controller
    {
        public readonly ICalculator _calculator;
        public LoanCalculatorController(ICalculator calculator)
        {
            _calculator = calculator;
        }

        [HttpPost("compare")]
        [ProducesResponseType(typeof(ComparisonRequest), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public async Task<IActionResult> Post([FromBody] ComparisonRequest request, CancellationToken token = default)       
            =>  Ok( await _calculator.Compare(request, token) );       
    }
}

