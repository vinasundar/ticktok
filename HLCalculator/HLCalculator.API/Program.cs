﻿using System.Text.Json;
using HLCalculator.API;
using HLCalculator.Business;
using HLCalculator.Services.Http.Filter;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers(options =>
     options.Filters.Add<HttpGlobalExceptionFilter>()
).AddJsonOptions(options =>
{
    options.JsonSerializerOptions.PropertyNamingPolicy = null;
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options =>
{
    //Should be updated to restrict to specfic origin / methods
    options.AddPolicy(AppConstants.CorsPolicy,
        builder =>
        {
            builder.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .SetIsOriginAllowed(a => _ = true);
        });
});

builder.Services.RegisterDomainServices(builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseCors(AppConstants.CorsPolicy);

app.UseAuthorization();

app.MapControllers();

app.Run();

public partial class Program { }

