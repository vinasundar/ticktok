﻿using FluentValidation;
using System;
using HLCalculator.Domain.LoanComparison;
using HLCalculator.Services.Http;
using HLCalculator.Services.Http.Exceptions;

namespace HLCalculator.Business;
public interface ICalculator
{
    Task<ComparisonResponse> Compare(ComparisonRequest request, CancellationToken token);
}


public class LoanCalculator: ICalculator
{
    private readonly ILoanComparisonService _loanComparisonSvc;
    private readonly IValidator<ComparisonRequest> _validator;
    public LoanCalculator(ILoanComparisonService loanComparisonSvc, IValidator<ComparisonRequest> validator)
    {
        _loanComparisonSvc = loanComparisonSvc;
        _validator = validator;
    }

    public async Task<ComparisonResponse?> Compare(ComparisonRequest request, CancellationToken token)
    {
        var validation = await _validator.ValidateAsync(request).ConfigureAwait(false);

        if (!validation.IsValid)
            throw new HLBadRequestException("Bad Request: Validation failure", validation.ToDictionary());         
        
        return await _loanComparisonSvc.Compare(request, token);
    }
}

