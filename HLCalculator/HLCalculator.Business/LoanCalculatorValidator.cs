﻿using System;
using FluentValidation;
using FluentValidation.Results;
using HLCalculator.Domain.LoanComparison;
namespace HLCalculator.Business;

public partial class LoanCalculatorValidator : AbstractValidator<ComparisonRequest>
{
	public LoanCalculatorValidator()
	{
		RuleFor(a => a.Lender).NotNull().NotEmpty();
        RuleFor(a => a.Merchant).NotNull().NotEmpty();
        RuleFor(a => a.RateType).NotNull().NotEmpty();
        RuleFor(a => a.RepaymentType).NotNull().NotEmpty();
        RuleFor(a => a.PropertyUsage).NotNull().NotEmpty();
        RuleFor(a => a.LoanTerm).Custom((a, context) => isValid(a, context));
        RuleFor(a => a.BorrowingAmount).Custom((a, context) => isValid(a, context));
        RuleFor(a => a.CustomerRate).Custom((a, context) => isValid(a, context));
    }

    protected override bool PreValidate(ValidationContext<ComparisonRequest> context, ValidationResult result)
    {
        if (context.InstanceToValidate == null)
        {
            result.Errors.Add(new ValidationFailure("", "Please ensure request is not null."));
            return false;
        }
        return true;
    }
}

public partial class LoanCalculatorValidator
{
    private bool isValid(long filter, ValidationContext<ComparisonRequest> context)
    {
        if (filter == 0)
        {
            context.AddFailure($"{context.DisplayName} Cannot be zero.");
            return false;
        }

        return true;
    }

    private bool isValid(decimal filter, ValidationContext<ComparisonRequest> context)
    {
        if (filter == 0)
        {
            context.AddFailure($"{context.DisplayName} Cannot be zero.");
            return false;
        }

        return true;
    }

    private bool isValid(int filter, ValidationContext<ComparisonRequest> context)
    {
        if (filter == 0)
        {
            context.AddFailure($"{context.DisplayName} Cannot be zero.");
            return false;
        }

        return true;
    }
}

