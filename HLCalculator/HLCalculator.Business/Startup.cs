﻿using System;
using FluentValidation;
using HLCalculator.Services.Startup;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HLCalculator.Business;

public static class Registration
{
    public static void RegisterDomainServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddValidatorsFromAssemblyContaining<LoanCalculatorValidator>();
        services.RegisterInfrastructure(configuration);
        services.AddScoped<ICalculator, LoanCalculator>();
    }
}


