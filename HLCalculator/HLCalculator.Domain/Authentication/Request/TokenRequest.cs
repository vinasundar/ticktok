﻿using System;


namespace HLCalculator.Domain.Authentication
{
	public class TokenRequest
	{
        [JsonProperty("scope")]
        public string Scope { get; set; }
        [JsonProperty("grant_type")]
        public string GrantType { get; set; } = "client_credentials";
    }
}

