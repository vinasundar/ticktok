﻿using System;
namespace HLCalculator.Domain.Authentication
{
	public class TokenResponse
	{
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }     
	}
}

