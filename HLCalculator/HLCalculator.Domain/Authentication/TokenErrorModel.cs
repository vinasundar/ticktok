﻿using System;
using Newtonsoft.Json.Linq;

namespace HLCalculator.Domain.Authentication
{
	public class TokenErrorModel
	{
		public JToken Errors { get; set; }
	}
}

