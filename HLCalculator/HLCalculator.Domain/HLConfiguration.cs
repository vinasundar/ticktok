﻿using System;
using static System.Formats.Asn1.AsnWriter;

namespace HLCalculator.Domain
{
    public class HLBaseConfiguration
    {       
        public string BaseUrl { get; set; }
    }

    public sealed class HLAuthConfiguration: HLBaseConfiguration
    {
        public string Scope { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public bool IsValid => !string.IsNullOrEmpty(BaseUrl) &&
                              !string.IsNullOrEmpty(Scope) &&
                              !string.IsNullOrEmpty(ClientId) &&
                              !string.IsNullOrEmpty(ClientSecret);
    }

    public sealed class HLCalculatorConfiguration : HLBaseConfiguration
    {
        public bool IsValid => !string.IsNullOrEmpty(BaseUrl);
    }

}

