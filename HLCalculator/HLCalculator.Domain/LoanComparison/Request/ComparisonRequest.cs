﻿using System;
namespace HLCalculator.Domain.LoanComparison
{
	public class ComparisonRequest
    {
        public string Merchant { get; set; }
        public string Lender { get; set; }
        public string RateType { get; set; }
        public string RepaymentType { get; set; }
        public string PropertyUsage { get; set; }
        public decimal CustomerRate { get; set; }
        public int LoanTerm { get; set; }
        public long BorrowingAmount { get; set; }
        public string RateTerm { get; set; }
	}
}

