﻿using System;
using Newtonsoft.Json.Linq;

namespace HLCalculator.Domain.LoanComparison
{
	public sealed class ComparisonResponse
    {
		public decimal MonthlyRepaymentDifference { get; set; }
        public decimal PayPeriodDifference { get; set; }
        public decimal TotalSaved { get; set; }
        public string RequestId { get; set; }
        public JToken Messages { get; set; }
    }
}