﻿using System;
using System.Security;
using System.Threading;

namespace HLCalculator.Services.Adapter
{
	public interface ITokenCache
    {
        (string token, long? expiresAt) Get { get; }
        void Set(string token, long expiresAt, string identifier);     
    }

    public sealed class TokenCache: ITokenCache
    {		
		private readonly SemaphoreSlim _lock ;
        private string _token { get; set; }       
        private long? _expiresAt { get; set; }
        public string Identifier { get; private set; }

        public TokenCache()
		{
            _lock = new SemaphoreSlim(0, 1);
        }

		public void Set(string token, long expiresAt, string identifier)
		{
            _lock.WaitAsync();

			try
			{
				_token = token;
				_expiresAt = expiresAt;
                Identifier = identifier;
            }			
			finally
			{
				_lock.Release(1);
            }
        }

        public (string token, long? expiresAt) Get => new (_token, _expiresAt);
    }
}

