﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading;
using HLCalculator.Services.Http;
using HLCalculator.Services.Http.Exceptions;
using Newtonsoft.Json.Linq;

namespace HLCalculator.Services.Adapter
{
	public interface ITokenHandler
	{
		 ValueTask<string> GetToken(CancellationToken cancellationToken);
    }


    public class TokenHandler: ITokenHandler
    {
		private readonly ITokenCache _tokenCache;
		private readonly ITokenService _tokenService;

        private long epochNow => DateTimeOffset.Now.ToUnixTimeSeconds();
		private const long before = 300;
        public TokenHandler(ITokenCache tokenCache, ITokenService tokenService)
		{
			_tokenCache = tokenCache;
			_tokenService = tokenService;
        }

		private void create(string token)
		{
            var handler = new JwtSecurityTokenHandler();
            var tokenObj = handler.ReadJwtToken(token);

			if (!long.TryParse(tokenObj?.Claims?.FirstOrDefault(a => a.Type == "exp").Value, out long expiresAt))
				throw new HLNotAuthorisedException("Not Authorised");

			_tokenCache.Set(token, expiresAt, tokenObj.Claims.FirstOrDefault(a => a.Type == "jti").Value);
		}

		public async ValueTask<string> GetToken(CancellationToken cancellationToken)
		{
			var tokenDets = _tokenCache.Get;
			if (tokenDets.expiresAt.HasValue && (epochNow - 300) > tokenDets.expiresAt.Value)
				return tokenDets.token;

			var request = await _tokenService.Get(cancellationToken).ConfigureAwait(false);
			create(request?.AccessToken);

            return request.AccessToken;
        }
	}
}

