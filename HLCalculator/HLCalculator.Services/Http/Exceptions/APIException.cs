﻿using System;
namespace HLCalculator.Services.Http.Exceptions
{
	public class APIException
	{
		public string StatusCode { get; set; }
        public string Message { get; set; }
    }
}

