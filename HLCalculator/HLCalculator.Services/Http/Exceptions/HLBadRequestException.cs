﻿using System;
namespace HLCalculator.Services.Http.Exceptions
{
	public class HLBadRequestException:Exception
	{
		public readonly IDictionary<String, String[]> _Errors;
		public HLBadRequestException(string message, IDictionary<String, String[]> errors)
			: base(message)
		{
            _Errors = errors;
        }
	}
}

