﻿using System;
namespace HLCalculator.Services.Http.Exceptions
{
	public class HLInternalServerException:Exception
	{
		public HLInternalServerException(string message,Exception ex):base(message, ex)
		{
		}
	}
}

