﻿using System;
namespace HLCalculator.Services.Http.Exceptions
{
    public class HLNotAuthorisedException : Exception
    {
        public HLNotAuthorisedException(string message) : base(message)
        {
        }
    }
}

