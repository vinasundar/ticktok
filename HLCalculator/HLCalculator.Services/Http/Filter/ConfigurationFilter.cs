﻿using System;
using HLCalculator.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;

namespace HLCalculator.Services.Http.Filter;

public sealed class HLAuthConfigurationFilter : IStartupFilter
{
    private readonly IOptions<HLAuthConfiguration> options;
    public HLAuthConfigurationFilter(IOptions<HLAuthConfiguration> _options)
    {
        options = _options;
    }

    public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
    {
        if (!options.Value.IsValid)
            throw new ArgumentNullException("Property Configuration not defined.");

        return next;
    }
}

public sealed class HLCalculatorConfigurationFilter : IStartupFilter
{
    private readonly IOptions<HLCalculatorConfiguration> options;
    public HLCalculatorConfigurationFilter(IOptions<HLCalculatorConfiguration> _options)
    {
        options = _options;
    }

    public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
    {
        if (!options.Value.IsValid)
            throw new ArgumentNullException("Property Configuration not defined.");

        return next;
    }
}


