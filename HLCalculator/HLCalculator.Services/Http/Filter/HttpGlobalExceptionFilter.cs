﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using HLCalculator.Services.Http.Exceptions;

namespace HLCalculator.Services.Http.Filter
{
	public sealed class HttpGlobalExceptionFilter: IExceptionFilter
    {
        private readonly ILogger<HttpGlobalExceptionFilter> _logger;
        private readonly IHttpContextAccessor _contextAccessor;
        public HttpGlobalExceptionFilter(ILogger<HttpGlobalExceptionFilter> logger, IHttpContextAccessor httpContext)
		{
            _logger = logger;
            _contextAccessor = httpContext;
        }

        private string? correlationId => _contextAccessor?.HttpContext?.Request?.Headers["correlation-id"];

        public void OnException(ExceptionContext context)
        {
            _logger.LogError(new EventId(context.Exception.HResult),
                    context.Exception,
                    $"{context.Exception.ToString()},correlationId :{correlationId ?? Guid.NewGuid().ToString()}");

            switch (context.Exception)
            {
                case HLBadRequestException:
                    ValidationException.Handle(context);
                    break;
                case HLNotAuthorisedException:
                    UnAuthorisedException.Handle(context);
                    break;
                case HLInternalServerException:               
                default:
                    InternalServerException.Handle(context);
                    break;
            }          
        }
    }

    internal sealed class ValidationException 
    {
        public static void Handle(ExceptionContext context)
        {
            var problemDetails = new ValidationProblemDetails()
            {
                Instance = context.HttpContext.Request.Path,
                Status = StatusCodes.Status400BadRequest,
                Detail = "Please refer to the errors property for additional details."
            };

            var exception = context.Exception as HLBadRequestException;

            problemDetails.Errors.Add("Validations",
                        exception?._Errors.Select(a => $"{a.Key},{string.Join("," ,a.Value)}").ToArray() ??
                                new string[] { });

            context.Result = new BadRequestObjectResult(problemDetails);
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
        }
    }

    internal sealed class InternalServerException 
    {
        public static void Handle(ExceptionContext context)
        {
            var exception = context.Exception as HLInternalServerException;
            var problemDetails = new ProblemDetails()
            {
                Instance = context.HttpContext.Request.Path,
                Status = StatusCodes.Status500InternalServerError,
                Detail = "Internal server Error."
            };

            context.Result = new ObjectResult(problemDetails);
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
        }
    }

    internal sealed class UnAuthorisedException
    {
        public static void Handle(ExceptionContext context)
        {
            var exception = context.Exception as HLNotAuthorisedException;
            var problemDetails = new ProblemDetails()
            {
                Instance = context.HttpContext.Request.Path,
                Status = StatusCodes.Status401Unauthorized,
                Detail = "Not Authorised to perform operation."
            };

            context.Result = new ObjectResult(problemDetails);
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
        }
    }
}

