﻿using System;
using HLCalculator.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace HLCalculator.Services.Http.Handler;

public class AuthorizationHandler : DelegatingHandler
{   
    private HLAuthConfiguration _config;

    public AuthorizationHandler(IOptions<HLAuthConfiguration> config)
    {
        _config = config.Value;        
    }

    private string encode(string content)
    {
        var bytes = System.Text.Encoding.UTF8.GetBytes(content);
        return $"Basic {System.Convert.ToBase64String(bytes)}";
    }
   
    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
        CancellationToken cancellationToken)
    {
        var code = encode($"{_config.ClientId}:{_config.ClientSecret}");
        request.Headers.Add("Authorization", code);
        return await base.SendAsync(request, cancellationToken);
    }

}


