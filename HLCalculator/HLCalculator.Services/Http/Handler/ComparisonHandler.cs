﻿using System;
using HLCalculator.Services.Adapter;
using Microsoft.IdentityModel.Tokens;

namespace HLCalculator.Services.Http.Handler
{
	public class ComparisonHandler : DelegatingHandler
    {
		private readonly ITokenHandler _tokenHandler;

        public ComparisonHandler(ITokenHandler tokenHandler)
		{
			_tokenHandler = tokenHandler;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
        CancellationToken cancellationToken)
        {
            var token = await _tokenHandler.GetToken(cancellationToken);
            request.Headers.Add("Authorization", $"Bearer {token}");
            return await base.SendAsync(request, cancellationToken);
        }
    }
}

