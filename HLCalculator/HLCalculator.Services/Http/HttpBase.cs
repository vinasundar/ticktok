﻿using System;
using System.Net.Mime;

namespace HLCalculator.Services.Http
{
	public class HttpBase
    {
        protected readonly HttpClient _client;

        public HttpBase(HttpClient client)
        {
            _client = client;
        }

        protected async Task<TResponse?> Get<TResponse>(string url, CancellationToken token = default)
        => await this.ExecuteGetAsync<TResponse>(url,
           new Dictionary<string, string>(), token).ConfigureAwait(false);

        protected async Task<TResponse?> Get<Request, TResponse>(string url, Dictionary<string, string> headers,
        CancellationToken token = default)
        => await this.ExecuteGetAsync<TResponse>(url, headers, token)
                        .ConfigureAwait(false);

        protected async Task<TResponse?> Post<Request, TResponse>(string url, Request request,
        string contentType, Dictionary<string, string> headers,
        CancellationToken token = default)
        => await this.ExecutePostAsync<Request, TResponse>(url, request, contentType, headers, token)
                .ConfigureAwait(false);

        private async Task<TResponse?> ExecuteGetAsync<TResponse>(string url,
        Dictionary<string, string> headers,
        CancellationToken token)
        {
            using (var request = new HttpRequestMessage(HttpMethod.Get, url))
            {
                _client.Addheaders(headers, request);
                return await ExecuteAsync<TResponse>(request, url, token).ConfigureAwait(false);
            }
        }

        private async Task<TResponse?> ExecutePostAsync<Request, TResponse>(string url,
        Request model, string contentType,
        Dictionary<string, string> headers,
        CancellationToken token)
        {
            using (var memStream = new MemoryStream())
            {
                memStream.SerializeToJsonStream(model);

                using (var request = new HttpRequestMessage(HttpMethod.Post, url))
                {
                    using (var stream = new StreamContent(memStream))
                    {
                        request.Content = stream;
                        request.Content.Headers.ContentType = HttpUtility.CustomMediaTypeHeaderValue(
                                                                    string.IsNullOrEmpty(contentType)
                                                                    ? HttpConstants.TypeApplicationJson : contentType);
                        _client.Addheaders(headers, request);
                        return await ExecuteAsync<TResponse>(request, url, token).ConfigureAwait(false);
                    }
                }
            }
        }

        protected async Task<TResponse?> ExecuteFormEncodedPostAsync<TResponse>(string url,
        KeyValuePair<string, string>[] model, string contentType,
        Dictionary<string, string> headers,
        CancellationToken token = default)
        {
            using (var request = new HttpRequestMessage(HttpMethod.Post, url))
            {
                request.Content = new FormUrlEncodedContent(model);
                request.Content.Headers.ContentType = HttpUtility.CustomMediaTypeHeaderValue(HttpConstants.TypeFormEncoded);
                _client.Addheaders(new Dictionary<string, string>(), request);
                return await ExecuteAsync<TResponse>(request, url, token).ConfigureAwait(false);
            }
        }

        private async Task<TResponse?> ExecuteAsync<TResponse>(HttpRequestMessage request,
        string url, CancellationToken token)
        {
            using (var response = await _client
                            .SendAsync(request, HttpCompletionOption.ResponseHeadersRead, token))
            {
                await response.Verify();
                var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                var _response = responseStream.ReadAndDeserializeFromJson<TResponse>();

                return _response;
            }
        }
    }
}

