﻿using System;
namespace HLCalculator.Services.Http
{
	public class HttpConstants
	{
		public const string UserAgent = "TikTok Test";
		public const string TypeFormEncoded = "application/x-www-form-urlencoded";
        public const string TypeApplicationJson = "application/json";
        public const string LoanCompareAction = "getloancomparison";
        public const string OAuthGrantType = "client_credentials";
    }
}

