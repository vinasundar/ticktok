﻿
using System.Net;
using HLCalculator.Services.Http.Exceptions;
using HLCalculator.Domain.Authentication;
using System.Net.Http.Headers;

namespace HLCalculator.Services.Http;

public static class HttpUtility
{
    public static void Addheaders(this HttpClient client, Dictionary<string, string> headers,
        HttpRequestMessage request)
    {
        client.DefaultRequestHeaders.Clear();
        client.DefaultRequestHeaders.UserAgent.ParseAdd(HttpConstants.UserAgent);
            
        foreach (var header in headers)
        {
            request.Headers.Add(header.Key, header.Value);
        }
    }

    public static MediaTypeHeaderValue CustomMediaTypeHeaderValue(string type) => new MediaTypeHeaderValue(type);

    public static async ValueTask Verify(this HttpResponseMessage response)
	{
        if (response.IsSuccessStatusCode)
            return;

        switch (response.StatusCode)
        {
            case HttpStatusCode.UnprocessableEntity:
            case HttpStatusCode.BadRequest:
                //var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                //var exceptionDetails = responseStream.ReadAndDeserializeFromJson<TokenErrorModel>();
                //var errors = exceptionDetails?.Errors.ToObject<IDictionary<string, string[]>>();

                var error = await response.Content.ReadAsStringAsync();
                var errors = new Dictionary<string, string[]>();
                errors.Add("context", new string[] { error });               
                throw new HLBadRequestException("Bad Request: Validation failed", errors);
            case HttpStatusCode.Unauthorized:
                throw new HLNotAuthorisedException("Not Authorised.");
            case HttpStatusCode.InternalServerError:
            default:
                throw new HLInternalServerException("Internal Server Error.",
                    new Exception(response.Content.ToJson()));
        }
    }

        
}

