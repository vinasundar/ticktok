﻿using System;
using HLCalculator.Domain;
using HLCalculator.Domain.Authentication;
using HLCalculator.Domain.LoanComparison;
using Microsoft.Extensions.Options;

namespace HLCalculator.Services.Http;

public interface ILoanComparisonService
{
    Task<ComparisonResponse?> Compare(ComparisonRequest request, CancellationToken token);
}

public class LoanComparisonService: HttpBase, ILoanComparisonService
{
    private HLCalculatorConfiguration _config;

    public LoanComparisonService(HttpClient client, IOptions<HLCalculatorConfiguration> config)
        : base(client)
    {
        _config = config.Value;
    }

    public async Task<ComparisonResponse?> Compare(ComparisonRequest request, CancellationToken token)
	{
        return await base.Post<ComparisonRequest, ComparisonResponse>(HttpConstants.LoanCompareAction, request,
           HttpConstants.TypeApplicationJson, new Dictionary<string, string>(),token: token);
    }
}


