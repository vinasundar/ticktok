﻿using System;
using System.Net.Mime;
using System.Reflection.PortableExecutable;
using HLCalculator.Domain;
using HLCalculator.Domain.Authentication;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace HLCalculator.Services.Http;

public interface ITokenService
{
	Task<TokenResponse?> Get(CancellationToken token);
}

public class TokenService: HttpBase , ITokenService
{	
    private HLAuthConfiguration _config;

    public TokenService(HttpClient client, IOptions<HLAuthConfiguration> config)
		:base(client)
	{
		_config = config.Value;
    }

	public async Task<TokenResponse?> Get(CancellationToken token)
	{
        var encodedBody = new[]
        {
            new KeyValuePair<string, string>("grant_type", HttpConstants.OAuthGrantType),
            new KeyValuePair<string, string>("scope", _config.Scope),
        };

        return await base.ExecuteFormEncodedPostAsync<TokenResponse>(string.Empty, encodedBody
            , HttpConstants.TypeFormEncoded, new Dictionary<string, string>() );
	}
}


