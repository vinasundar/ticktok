﻿using System;
using System.Net;
using HLCalculator.Domain;
using HLCalculator.Services.Adapter;
using HLCalculator.Services.Http;
using HLCalculator.Services.Http.Filter;
using HLCalculator.Services.Http.Handler;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HLCalculator.Services.Startup
{
	public static class Registration
	{
		public static void RegisterInfrastructure(this IServiceCollection services, IConfiguration configuration)
		{
            RegisterConfigurations(services, configuration);
            RegisterClients(services, configuration);
        }

        private static void RegisterConfigurations(IServiceCollection services, IConfiguration configuration)
        {

            services.Configure<HLAuthConfiguration>(configuration.GetSection("Service:Authentication"));
            services.AddSingleton<IStartupFilter, HLAuthConfigurationFilter>();

            services.Configure<HLCalculatorConfiguration>(configuration.GetSection("Service:Calculator"));
            services.AddSingleton<IStartupFilter, HLCalculatorConfigurationFilter>();
        }

        private static void RegisterClients(IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpContextAccessor();
            services.AddSingleton<ITokenCache, TokenCache>();
            services.AddScoped<ITokenHandler, TokenHandler>();

            services.AddTransient<AuthorizationHandler>();
            services.AddTransient<ComparisonHandler>();

            var AuthbaseUrl = configuration.GetValue<string>("Service:Authentication:BaseUrl");

            services.AddHttpClient<ITokenService, TokenService>(a =>
            {
                a.BaseAddress = new Uri(AuthbaseUrl);
            })
            .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                AllowAutoRedirect = false,
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
            })
            .AddHttpMessageHandler<AuthorizationHandler>();

            var ComparebaseUrl = configuration.GetValue<string>("Service:Calculator:BaseUrl");

            services.AddHttpClient<ILoanComparisonService, LoanComparisonService>(a =>
            {
                a.BaseAddress = new Uri(ComparebaseUrl);
            })
            .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                AllowAutoRedirect = false,
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
            })
            .AddHttpMessageHandler<ComparisonHandler>();
        }

    }
}

