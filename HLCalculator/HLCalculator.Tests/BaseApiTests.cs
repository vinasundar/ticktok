﻿using System;
using System.Net;
using System.Net.Http.Headers;
using System.Reflection;
using Xunit.Abstractions;

namespace HLCalculator.Tests
{
	public class BaseApiTests
	{
        protected CalculatorWebApplicationFactory<Program> _factory;
        protected readonly Mock<HttpMessageHandler> _mockMsgHandler;
        protected readonly ITestOutputHelper _output;
        public BaseApiTests(CalculatorWebApplicationFactory<Program> factory,ITestOutputHelper output)
		{
            _mockMsgHandler = new();
            _factory = factory;
            _output = output;
        }

        private async Task<string> ContentsAsString(string path) =>
            await File.ReadAllTextAsync(path);

        protected async Task<string> getContent(string code, string folder)
        {
            return await ContentsAsString( (code switch
            {
                null or "" => throw new ArgumentNullException("Test source file path not defined."),
                _ => Path.Combine(new FileInfo(Assembly.GetExecutingAssembly().Location).Directory.FullName, folder, $"{code}.json")
            }));
        }

        public void addHeaders(HttpClient client, HttpRequestMessage request)
        {
            client.DefaultRequestHeaders.Clear();
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.UserAgent.ParseAdd(HttpConstants.UserAgent);
        }
        
        protected  async Task<(HttpClient client, string expected)> utilityBuilder(string code,
            HttpStatusCode httpStatusCode, Action<string>? retryAction = null)
        {
            var expectedResponse = await getContent(code, "ResponseMock").ConfigureAwait(false);

            var apiResponse = new HttpResponseMessage
            {
                Content = new StringContent(expectedResponse),
                StatusCode = httpStatusCode
            };

            _mockMsgHandler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
                       ItExpr.IsAny<CancellationToken>()).ReturnsAsync(apiResponse);

            var _client = _factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    builder.UseSetting("urls", "http://localhost");

                    var cacheAdapter = services.SingleOrDefault(
                        d => d.ServiceType ==
                            typeof(ITokenCache));

                    services.Remove(cacheAdapter);

                    services.AddSingleton<ITokenCache, MockTokenCache>();

                    var httpClient = services.SingleOrDefault(
                        d => d.ServiceType ==
                            typeof(ILoanComparisonService));

                    services.Remove(httpClient);

                    services.AddHttpClient<ILoanComparisonService, LoanComparisonService>(a =>
                    {
                        a.BaseAddress = new Uri("https://localhost:5050");
                    })
                   .ConfigurePrimaryHttpMessageHandler(() => _mockMsgHandler.Object);
                });
            })
            .CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });

            return (_client, expectedResponse);
        }
    }
}

