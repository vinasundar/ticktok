﻿using System;
using System.Net;
using System.Reflection;
using System.Text;
using HLCalculator.Services.Helpers;

using Newtonsoft.Json.Linq;
using Xunit.Abstractions;

namespace HLCalculator.Tests;

	public class SavingsApiTest : BaseApiTests, IClassFixture<CalculatorWebApplicationFactory<Program>>
    {
        public SavingsApiTest(CalculatorWebApplicationFactory<Program> factory, ITestOutputHelper output) :base(factory, output)
		{         
        }

        [Fact]
        public async Task LoanComparison_Request_Returns_Success_Response()
        {
            //Arrange
            var request = await getContent("apirequest_200", "RequestMock").ConfigureAwait(false);
            var utility = await utilityBuilder("loanapi_200response", HttpStatusCode.OK).ConfigureAwait(false);
            using var client = utility.client;

            //Act
            var requestMsg = new HttpRequestMessage(HttpMethod.Post, "api/v1/calculator/compare");
            addHeaders(client, requestMsg);
            requestMsg.Content = new StringContent(request, Encoding.UTF8, "application/json");
            var response = client.SendAsync(requestMsg).ConfigureAwait(false).GetAwaiter().GetResult();

            //Gaurd
            Assert.NotNull(response);

            var actualResponse = (await response.Content.ReadAsStringAsync().ConfigureAwait(false));

            //Assert
            Assert.NotNull(actualResponse);
            Assert.True(response.StatusCode == HttpStatusCode.OK);
            var actual = actualResponse.ToModel<JToken>(formatting: Formatting.Indented);
            var expected = utility.expected.ToModel<JToken>(formatting: Formatting.Indented);

            _output.WriteLine(actualResponse);
            _output.WriteLine(utility.expected);

            Assert.True(JToken.DeepEquals(actual, expected));
        }

        [Fact]
        public async Task LoanComparison_Request_Service_Failure_Returns_BadRequest()
        {
            //Arrange
            var request = await getContent("apirequest_400response", "RequestMock").ConfigureAwait(false);
            var utility = await utilityBuilder("apiresponse_400badRequest", HttpStatusCode.BadRequest).ConfigureAwait(false);
            using var client = utility.client;

            //Act
            var requestMsg = new HttpRequestMessage(HttpMethod.Post, "api/v1/calculator/compare");
            addHeaders(client, requestMsg);
            requestMsg.Content = new StringContent(request, Encoding.UTF8, "application/json");
            var response = client.SendAsync(requestMsg).ConfigureAwait(false).GetAwaiter().GetResult();

            //Gaurd
            Assert.NotNull(response);

            var errorModel = (await response.Content.ReadAsStringAsync().ConfigureAwait(false));

            //Assert          
            Assert.NotNull(errorModel);
            Assert.True(response.StatusCode == HttpStatusCode.BadRequest);
        }
}

