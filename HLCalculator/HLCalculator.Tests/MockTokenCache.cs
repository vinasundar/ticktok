﻿using System;
using HLCalculator.Services.Adapter;

namespace HLCalculator.Tests
{
    public sealed class MockTokenCache : ITokenCache
    {
       
        private string _token { get; set; } = TestConstants.Token;
        private long? _expiresAt { get; set; } = DateTimeOffset.Now.ToUnixTimeSeconds() + 3600;
        public string Identifier { get; private set; } = Guid.NewGuid().ToString();
     
        public void Set(string token, long expiresAt, string identifier)
        {        
        }

        public (string token, long? expiresAt) Get => new(_token, _expiresAt);
    }

    internal class TestConstants
    {
        public const string Token = "eyJraWQiOiJRYXNvNkVMTjZmblRROXpMTzhqYytTdmxBT0NcL2I0UDdaaktKVHFIWTd4OD0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI2bWtwazVlZ3VnZTJyMW1laTA4cTBkNXVyMiIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXBpLnN0YWdlLnRpY3RvYy5haVwvZGV2dGFzayIsImF1dGhfdGltZSI6MTY3NTA2MDUyMCwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLmFwLXNvdXRoZWFzdC0yLmFtYXpvbmF3cy5jb21cL2FwLXNvdXRoZWFzdC0yX05iTlNZSkdWQyIsImV4cCI6MTY3NTA2NDEyMCwiaWF0IjoxNjc1MDYwNTIwLCJ2ZXJzaW9uIjoyLCJqdGkiOiIzOTAyYTI4Ni05Y2NhLTQxYTItYjE3NC04ZmE2OTFiZTBkNWYiLCJjbGllbnRfaWQiOiI2bWtwazVlZ3VnZTJyMW1laTA4cTBkNXVyMiJ9.QgMr9gbwahz0PQAKN7XsRPnRyNM00f4JTcSMKUFJxwEOUL5Gtzcv65rdrqmz0mqZYdqdX-tGkUAXKCP1oc2iLVZlUZdXlN2uSZAOaP-QlETLbNdeZLpQQZ70ww4iJZM3TWki6devcFsQvz3RXrNycwY-ZSbk-JnOq5A516158oyXTb-w_lP6063OGPtCUgLe_1KB3HUP7fSkwgFt4qXJcABC78wYLcH0Jswd4CoWiG7W_aWLMo5YjE9oFZghE4fks3Di9OJv9TjrRF-ceUmGuiosHOTXClnLe79WvDFkOwThROIjVx2nUYfLEN-g0do1qOoen-ysJ51KuEpB7LFDsA";
    }
}

