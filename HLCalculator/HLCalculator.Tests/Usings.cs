﻿global using Xunit;
global using Moq;
global using Moq.Protected;
global using Newtonsoft.Json;
global using Microsoft.VisualStudio.TestPlatform.TestHost;
global using HLCalculator.Services.Adapter;
global using HLCalculator.Services.Http;
global using Microsoft.AspNetCore.Mvc.Testing;
global using Microsoft.AspNetCore.TestHost;
global using Microsoft.Extensions.DependencyInjection;

