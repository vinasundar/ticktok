﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HLCalculator.Tests
{
	public class CalculatorWebApplicationFactory<TProgram>
    : WebApplicationFactory<TProgram> where TProgram : class
    {
		public CalculatorWebApplicationFactory()
		{
		}

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration((hostContext, configApp) =>
            {
                var env = hostContext.HostingEnvironment;
                configApp.AddJsonFile("appsettings.json", optional: false);               
                configApp.AddEnvironmentVariables();
            });

            builder.UseEnvironment("Development");
        }
    }
}

